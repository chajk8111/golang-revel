package controllers

import (
	"fmt"
	"myapp/app/models"
	
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	r "github.com/revel/revel"
)

var DB *gorm.DB

func init() {
	var err error
	DB, err = gorm.Open("mysql", "root:0000@tcp(127.0.0.1:3306)/myapp?parseTime=true")
	if err != nil {
		panic(fmt.Sprintf("gagal terkoneksi kedatabase, %v", err))
	}
	DB.AutoMigrate(&models.User{})
}

type ModelController struct {
	*r.Controller
	Orm *gorm.DB
}

func (c *ModelController) Begin() r.Result {
	c.Orm = DB
	return nil
}

// func (c *ModelController) CreateTable() r.Result {
// 	c.Orm.CreateTable(models.User{})
// 	return nil
// }
