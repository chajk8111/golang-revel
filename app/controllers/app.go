package controllers

import (
	"myapp/app/models"

	"github.com/revel/revel"
)

type App struct {
	ModelController
}

func (c App) Index() revel.Result {
	var users []models.User
	user := c.Orm.Find(&users)
	if user.Error != nil {
		panic(user.Error)
	}
	// fmt.Println(users)
	return c.Render(users)
}

func (c App) New() revel.Result {
	return c.Render()
}

func (c App) Create(username string, email string) revel.Result {
	user := models.User{Username: username, Email: email}
	c.Orm.Save(&user)
	return c.Redirect(App.Index)
}

func (c App) Show(id int) revel.Result {
	var users models.User
	user := c.Orm.First(&users, id)
	if user.Error != nil {
		panic(user.Error)
	}
	return c.Render(users)
}

func (c App) Edit(id int) revel.Result {
	var users models.User
	user := c.Orm.First(&users, id)
	if user.Error != nil {
		panic(user.Error)
	}
	return c.Render(users)
}

func (c App) Update(id int, username string, email string) revel.Result {
	users := models.User{}
	c.Orm.Model(&users).Where("ID=?", id).Updates(map[string]interface{}{"Username": username, "Email": email})
	return c.Redirect(App.Index)
}

func (c App) Delete(id int) revel.Result {
	user := models.User{}
	c.Orm.Where("ID=?", id).Delete(&user)
	return c.Redirect(App.Index)
}
